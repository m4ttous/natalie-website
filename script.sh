#!/bin/bash

# # home page

cat templates/header.html | sed 's/{{ public_path }}//' > public/index.html
cat pages/home.html >> public/index.html
cat templates/footer.html >> public/index.html

# contact page

mkdir public/contact

cat templates/header.html | sed 's/{{ public_path }}/\.\.\//' > public/contact/index.html
cat pages/contact.html >> public/contact/index.html
cat templates/footer.html >> public/contact/index.html

# blog page

mkdir public/blog

cat templates/header.html | sed 's/{{ public_path }}/\.\.\//' > public/blog/index.html
cat pages/blog.html >> public/blog/index.html
cat templates/footer.html >> public/blog/index.html

# cat public/test.html | sed 's/{{ public_path }}/wibble/' > public/test2.html

echo "script finished"
